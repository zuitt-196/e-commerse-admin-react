import SideBar from "./components/sidebar/SideBar";
import Topbar from "./components/topbar/Topbar";
import "./App.css"
import Home from "./page/home/Home";
import {
  BrowserRouter,
  Route,
  Routes,
} from "react-router-dom";
import UserList from "./page/userList/UserList";
import User from "./page/user/User";
import NewUser from "./page/newUser/NewUser";
import ProductList from "./page/produtList/ProductList";
import Product from "./page/product/Product";
import NewProduct from "./page/newProduct/NewProduct";

function App() {
  
  return (
    <BrowserRouter>

          <div>
          <Topbar/>
            <div className="container">
                        <SideBar/>
                        <Routes>
                          <Route exact path="/" element={<Home/>}/>
                          <Route path="/userlist" element={<UserList/>}/>
                          <Route path="/user/:id" element={<User/>}/>
                          <Route path="/newUser" element={<NewUser/>}/>
                          <Route path="/prductlist" element={<ProductList/>}/>
                          <Route path="/product/:id" element={<Product/>}/>
                          <Route path="/newproduct" element={<NewProduct/>}/>


                        </Routes>
                  </div>
          </div>
    </BrowserRouter>
  );
}

export default App;
