import React from 'react'
import './Chart.css'
import { LineChart, Line, XAxis, CartesianGrid, Tooltip,  ResponsiveContainer } from 'recharts';


const Chart = ({title, data, dataKey, grid}) => {
   
    console.log(data);
  return    (
    <div className="chart-container">
        <h3 className="chart-title">
                {title}
        </h3>
        <ResponsiveContainer width="100%" aspect={4/1}>
            <LineChart data={data}>
                    <XAxis dataKey="name" stroke="#5550bd"/>
                    <Line type="monotone" dataKey={dataKey} stroke="#5550bd"/>
                    <Tooltip/>
              {   grid && <CartesianGrid stroke='#e0dfdf' strokeDasharray="5 5 "/>}
            </LineChart>
        </ResponsiveContainer>
    </div>
  )
}

export default Chart