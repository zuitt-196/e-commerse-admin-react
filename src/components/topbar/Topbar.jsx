import React from 'react';
import './topbar.css'
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';

const Topbar = () => {
  return (
    <div className="topbar">
             <div className="topbar-wrapper">
            <div className="top-left">
                          <span className='logo'>VHONG</span>
                  </div>
          <div className="top-right">

                    <div className="topbar-icon-container">
                            <NotificationsNoneOutlinedIcon/>
                          <span className="top-icon-badge">1</span>
                    </div>

                    <div className="topbar-icon-container">
                            <LanguageOutlinedIcon/>
                          <span className="top-icon-badge">1</span>
                    </div>
                    
                    
                    <div className="topbar-icon-container">
                            <SettingsOutlinedIcon/>
                          
                    </div>

                    <img src="https://scontent.fceb1-3.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fceb1-3.fna&oh=00_AfCNATrP5ikDJeyrLwV-YOgFd4seYXDot19ycizK_XfA6g&oe=63E93111" alt="avatar1" className="top-avdatar" />
                    


             </div>
    </div>
</div>
  )
}

export default Topbar