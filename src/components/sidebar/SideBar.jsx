import React from 'react'
import './SideBar.css'
import LineStyleOutlinedIcon from '@material-ui/icons/LineStyleOutlined';
import TimelineOutlinedIcon from '@material-ui/icons/TimelineOutlined';
import TrendingUpOutlinedIcon from '@material-ui/icons/TrendingUpOutlined';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import StorefrontIcon from '@mui/icons-material/Storefront';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import MailIcon from '@mui/icons-material/Mail';
import DynamicFeedIcon from '@mui/icons-material/DynamicFeed';
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import ErrorIcon from '@mui/icons-material/Error';
import {Link} from "react-router-dom"

const SideBar = () => {
  return (
    <div className="sidebar">
            <div className="sidebar-wrapper">
                    <div className="sidebar-menu">

                            <h3 className="sidebar-title">
                                    Dashboard
                            </h3>
                                <ul className="sidebar-list">
                                    <li className="sibarListItem ">
                                                <LineStyleOutlinedIcon className="sidebar-icon"/>
                                                <samp>Home</samp>
                                            </li>    
                                            <li className="sibarListItem">
                                                <TimelineOutlinedIcon  className="sidebar-icon"/>
                                                <samp>Anaytics</samp>
                                            </li>  

                                                <li className="sibarListItem">
                                                <TrendingUpOutlinedIcon  className="sidebar-icon"/>
                                                <samp>Sales</samp>
                                            </li>        
                                </ul>
                    </div>
                    <div className="sidebar-menu">
                            <h3 className="sidebar-title">
                            Quick Menu
                            </h3>
                                <ul className="sidebar-list">

                                     <Link to="/userList" className='link-menu'>
                                          <li className="sibarListItem ">
                                                    <PersonOutlineIcon className="sidebar-icon"/>
                                                    <samp>Users</samp>
                                                </li>    
                                            </Link>

                                            <Link to="/prductlist"  className='link-menu'> 
                                                <li className="sibarListItem">
                                                    <StorefrontIcon  className="sidebar-icon"/>
                                                    <samp>Products</samp>
                                                </li>  

                                            </Link>

                                                <li className="sibarListItem">
                                                <AttachMoneyIcon   className="sidebar-icon"/>
                                                <samp>Transaction</samp>
                                            </li>        
                                </ul>
                    </div>
                    <div className="sidebar-menu">
                            <h3 className="sidebar-title">
                            Notification
                            </h3>
                                <ul className="sidebar-list">
                                    <li className="sibarListItem ">
                                                <MailIcon className="sidebar-icon"/>
                                                <samp>Mail</samp>
                                            </li>    
                                            <li className="sibarListItem">
                                                <DynamicFeedIcon className="sidebar-icon"/>
                                                <samp>Feedback</samp>
                                            </li>  

                                                <li className="sibarListItem">
                                                <TrendingUpOutlinedIcon  className="sidebar-icon"/>
                                                <samp>Sales</samp>
                                            </li>        
                                </ul>
                    </div>
                    <div className="sidebar-menu">
                            <h3 className="sidebar-title">
                                    Staft
                            </h3>
                                <ul className="sidebar-list">
                                    <li className="sibarListItem ">
                                                <WorkOutlineIcon className="sidebar-icon"/>
                                                <samp>Message</samp>
                                            </li>    
                                            <li className="sibarListItem">
                                                <TimelineOutlinedIcon  className="sidebar-icon"/>
                                                <samp>Anaytics</samp>
                                            </li>  

                                                <li className="sibarListItem">
                                                < ErrorIcon className="sidebar-icon"/>
                                                <samp>Reports</samp>
                                            </li>        
                                </ul>
                    </div>

                    
                 

            </div>
</div>  
  )
}

export default SideBar