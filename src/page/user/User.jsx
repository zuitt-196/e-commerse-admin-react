import React from 'react'
import "./User.css";
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import EmailIcon from '@mui/icons-material/Email';
import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import PublishIcon from '@mui/icons-material/Publish';
import {Link} from "react-router-dom";

const User = () => {
  return (
    <div className='user-container'>
            <div className="user-title-container">
                    <h1 className="user-title"> Edit User</h1>
                    <Link to="/newUser">  
                            <button className="user-btn">Create</button>
                    </Link>
                    
            </div>

            <div className="user-container-id">

                    {/* LEFTY USER SECTION */}
                    <div className="user-show">
                            <div className="user-show-top">
                                    <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfC7LRK3Zl_1nhM14VJVsfjAoC7q0yejS9mtlVst7vsPOA&oe=63EB2B51" alt="" className="user-show-img" />
                        

                                    <div className="user-showTop-title">
                                            <span className="user-show-name">Vhong bercasio</span>
                                            <span className="user-show-job">Web Devloper</span>
                                    </div>
                            </div>

                            <div className="user-show-botton">
                                    <span className='user-show-title'>
                                     Account Detaiils
                                    </span>
                                    <div className ="user-show-info">
                                            <PermIdentityIcon className="icon"/>
                                        <span className='user-show-info-title'>Vhong Bercasio</span>
                                    </div>
                                        
                                    <div className ="user-show-info">
                                            <CalendarTodayIcon className="icon"/>
                                        <span className='user-show-info-title'>04.17.1999</span>

                                    </div>

                                    <span className='user-show-title'>
                                     Contact Detaiils
                                    </span>
                                    <div className ="user-show-info">
                                            <PhoneAndroidIcon className="icon"/>
                                        <span className='user-show-info-title'> 09102764396</span>
                                    </div>
                                    
                                    <div className ="user-show-info">
                                            <EmailIcon className="icon"/>
                                        <span className='user-show-info-title'>Bercasio@gmal.com</span>
                                    </div>
                                    <div className ="user-show-info">
                                            <LocationSearchingIcon  className="icon"/>
                                        <span className='user-show-info-title'>Cumawas, bislig City</span>
                                    </div>
                                    
                                   
                        </div>
                </div>

                {/* RIGHT USER SECTION  */}
                    <div className="user-update">
                                <span className="user-update-title">Edit </span>
                                            <form className="user-update-form">
                                                    <div className="user-updated-left">
                                                                <div className="user-update-item">
                                                                        <label> User Name</label>
                                                                        <input type="text" placeholder='Vhong124' className='user-udpdate-input'/> 
                                                                </div>
                                                                <div className="user-update-item">
                                                                        <label>Full Name</label>
                                                                        <input type="text" placeholder='Vhong Bercasio' className='user-udpdate-input'/> 
                                                                        
                                                                    
                                                                </div>
                                                                <div className="user-update-item">
                                                                        <label>Email</label>
                                                                        <input type="email" placeholder='Vhong@gamil.com' className='user-udpdate-input'/> 
                                                                        
                                                                    
                                                                </div>
                                                                <div className="user-update-item">
                                                                        <label>Phone</label>
                                                                        <input type="text" placeholder='09102764396' className='user-udpdate-input'/> 
                                                                        
                                                                    
                                                                </div>
                                                                <div className="user-update-item">
                                                                        <label> Address</label>
                                                                        <input type="text" placeholder='Cumawas Bislig, City' className='user-udpdate-input'/> 
                                                                        
                                                                    
                                                                </div>
                                                            
                                                    </div>

                                                    <div className="user-updated-right">
                                                                <div className="user-updated-upload">
                                                                        <img src="https://scontent.fcgy2-2.fna.fbcdn.net/v/t39.30808-6/322572092_571665530976957_5019462618475599715_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFnxnyH-3T9ATTDgOwZVNB49uTgHOqOsr_25OAc6o6yv3RuB_hP_EZ7G8XB6q2-Q47_NadSPf-B_mzWpZOAxX8Z&_nc_ohc=Vou23tVOW60AX-dHu6z&_nc_ht=scontent.fcgy2-2.fna&oh=00_AfC7LRK3Zl_1nhM14VJVsfjAoC7q0yejS9mtlVst7vsPOA&oe=63EB2B51" alt="" className="user-upated-image" />
                                                                        <label htmlFor="file"> <PublishIcon/></label>
                                                                        <input type="file" id="file" style={{display:"none"}}/>
                                                                        
                                                                </div>
                                                                <button className="user-update-button">
                                                                            Update
                                                                 </button>
                                                               
                                                    </div>          
                                                    
                                    </form>
                    </div>
            </div>
    </div>
  )
}

export default User