import React from 'react'
import Chart from '../../components/chart/Chart'
import Featured from '../../components/featured/Featured'
import "./Home.css";
import {  userData  } from '../../dummtData';
import WidgetSmall from '../../components/widgetSm/WidgetSmall';
import WidgetLarge from '../../components/widgetLg/WidgetLarge';


const Home = () => {
  return (
    <div className="home-container">
        <Featured/>
        <Chart data={ userData } title="User Analytics" grid dataKey = "Active User"/>
        <div className="home-widgets">
              <WidgetSmall/>
              <WidgetLarge/>

        </div>
    </div>
  )
}

export default Home
