import { useState, useEffect } from 'react';
import './UserList.css'
import { DataGrid } from '@mui/x-data-grid';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { userRows } from '../../dummtData';
import {Link} from "react-router-dom"

const UserList = () => {


 
    //DEFINE THE STATE FOR USER ID
    const [userData, setUserData] = useState(userRows);
    // console.log(userData.id)
    const handleDelete = (id) => {  
        setUserData(userData.filter((data)=> data.id !== id))
    }
    
 

const columns = [
  { field: 'id', headerName: 'ID', width: 90 },
  { field: 'user', headerName: 'User', width: 200 , renderCell:(params) =>{
        return (
            <div className="user-list-User">
                    <img src={params.row.avatar} alt="" className ="user-list-image" />
                {params.row.username}
            </div>
               
        )
  }},
  { field: 'email', headerName: 'Last name', width: 200},

  {
    field: 'status',
    headerName: 'Status',
    width: 120,
  },
  {
    field: 'transaction',
    headerName: 'Transaction Volume',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,
  },

  {
    field: 'action',
    headerName: 'Action',
    width: 150,
    renderCell:(params) =>{ 
        return (
            <div className="user-list-action">
                <Link to = {"/user/" + params.row.id}>
                         <button className="user-list-Edit">Edit
                       
                         </button>
                            
                         {/* {console.log("id:", params.row.id)} */}
                    </Link>

                    <DeleteOutlineIcon className="user-list-delete" onClick={() =>handleDelete(params.row.id)}/>
            </div>

        )
    }
  }


];



  return (
    <div className='userlist-container'>
        <DataGrid
        rows={userData}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[8]}
        checkboxSelection
        disableSelectionOnClick
      />
      
    </div>
  )
}

export default UserList
