import React from 'react'
import './Product.css';
import {Link} from 'react-router-dom';
import Chart from '../../components/chart/Chart';
import { productData } from '../../dummtData';
import PublishIcon from '@mui/icons-material/Publish';

const Product = () => {
    console.log(productData)
  return (
    <div className='product-container'>
        <div className="product-title">
                <h1 className="product-title-">
                        Product
                </h1>
                <Link to="/newproduct">
                        <button className="product-button">Create</button>
                </Link>
        </div>
            <div className="product-top">
                <div className="product-top-left">
                            <Chart data={ productData } dataKey="Sales" title="Sales Performance"/>
                </div>
                <div className="product-top-right">
                        <div className="product-info-top">
                            <img src="https://lzd-img-global.slatic.net/g/p/3d0853fd2f0ec1379448a37b7c548c64.png_720x720q80.png_.webp" alt=""  className='produce-info-image'/>
                            <div className="product-info-name"> <span className="product-name">Nike Air</span></div>
                        </div>
                       
                        <div className="product-bottoom-top">
                                    <div className="product-info-item">
                                                <span className="product-infor-key">ID:</span>
                                                <span className="product-value">123</span>
                                    </div>

                                    <div className="product-info-item">
                                                <span className="product-infor-key">Sales:</span>
                                                <span className="product-value">$123</span>
                                    </div>

                                    <div className="product-info-item">
                                                <span className="product-infor-key">Active:</span>
                                                <span className="product-value">123</span>
                                    </div>

                                    <div className="product-info-item">
                                                <span className="product-infor-key">In Stack:</span>
                                                <span className="product-value">No</span>
                                    </div>
                        </div>
                </div>
            </div>

            <div className="product-bottom ">
                            <form  className="product-form">
                                    <div className="product-form-left">
                                            <label>Product Name:</label>
                                            <input type="text" placeholder="Nike"/>
                                            <label>In Stack:</label>
                                            <select name="inStock" id="idStock">
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
    
                                            <label>Active:</label>
                                            <select name="active" id="active">
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>

                                        
                                    </div>
                                    <div className="product-form-right">
                                        <div className="product-upload-image">
                                                <img src="https://lzd-img-global.slatic.net/g/p/3d0853fd2f0ec1379448a37b7c548c64.png_720x720q80.png_.webp" alt="" className="pruduct-upload-image" />
                                                <label for="file">
                                                    <PublishIcon/>
                                                </label>
                                                <input type="file" id="file" style={{display: "none"}}/>
                                        </div>
                                        <button className="upload-product-button">Update</button>
                                    </div>
                            </form>
                </div>
    
    </div>
  )
}

export default Product