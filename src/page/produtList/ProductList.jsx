import React,{useState}from 'react'
import "./ProductList.css"
import { DataGrid } from '@mui/x-data-grid';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { productRows } from '../../dummtData';
import {Link} from "react-router-dom"

const ProductList = () => {
    const [productData, setProductData] = useState(productRows) 

    const handleDelete = (id) => {  
        setProductData(productData.filter((data)=> data.id !== id))
    }
    

    
const columns = [
    { field: 'id', headerName: 'ID', width: 90 },
    { field: 'product', headerName: 'Product', width: 200 , renderCell:(params) =>{
          return (
              <div className="product-list-image">
                      <img src={params.row.image} alt="" className ="product-list-image" />
                  {params.row.name}
              </div>
                 
          )
    }},
    { field: 'stock', headerName: 'Stack', width: 200},
  
    {
      field: 'status',
      headerName: 'Status',
      width: 120,
    },
    {
      field: 'price',
      headerName: 'price Volume',
      width: 160,
    },
  
    {
      field: 'action',
      headerName: 'Action',
      width: 150,
      renderCell:(params) =>{ 
          return (
              <div className="product-list-action">
                  <Link to = {"/product/" + params.row.id}>
                           <button className="product-list-Edit">Edit
                         
                           </button>
                              
                           {/* {console.log("id:", params.row.id)} */}
                      </Link>
  
                      <DeleteOutlineIcon className="product-list-delete" onClick={() =>handleDelete(params.row.id)}/>
              </div>
  
          )
      }
    }
  
  
  ];

  return (
    <div className="product-list-container" >
 <DataGrid
        rows={productData}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[8]}
        checkboxSelection
        disableSelectionOnClick
      />    
        
        </div>
  )
}

export default ProductList